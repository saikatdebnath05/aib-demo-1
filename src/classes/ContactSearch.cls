public class ContactSearch {
	public static List<Contact> searchForContacts(String lName,String mPCode){
		List<Contact> myArray = new List<Contact>();
		String LastName = lName;
		String MCode = mPCode;
		myArray = [SELECT LastName,MailingPostalCode FROM Contact WHERE MailingPostalCode=:MCode AND LastName =: LastName];
		return myArray;
	}
}
({
        doInit: function(component, event, helper){
            var createCarRecord = $A.get("e.force:createRecord");
            if(createCarRecord){
                component.set("v.isNewAvailable",true);
            }else{
                component.set("v.isNewAvailable",false);
            }
        	helper.getCarType(component,event,helper);
    },
	onSearchClick : function(component, event, helper) {
		helper.handleOnClickEvent(component, event, helper);
	},
    onChange : function(component, event, helper) {
        
		var changeCarType = component.find("carType").get("v.value");
        alert(changeCarType+" has been selected.");
	},
    createCarRecord : function(component, event, helper) {
		var createCarRecord = $A.get("e.force:createRecord");
        createCarRecord.setParams({
            "entityApiName":"Car_Type__c" 
        });
        createCarRecord.fire();
	}
})
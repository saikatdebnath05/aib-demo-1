({
callServer : function(component,method,callback,params) {
        var action = component.get(method);
		if(params){
			action.setParams(params);
			console.log("Saikat");
		}
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

              callback.call(this,response.getReturnValue());

            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    }
})